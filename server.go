package main

import (
	"net/http"
	"sort"
	"strings"
	"fmt"
	"context"
	"log"
)

//----------------------------------------------------------------------------------------------------------------------

// HandlerFunc is the http.HandlerFunc type.
type HandlerFunc func(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request)



//----------------------------------------------------------------------------------------------------------------------

// App is the main instance, it contains the modifiers and the router.
// Create a new instance by using NewApp().
type Server struct {
	router          *router
	modifiers       Modifiers
	beforeModifiers Modifiers
	notFoundHandler HandlerFunc
}

// NewApp returns an App instance.
func NewServer() *Server {
	app := &Server{}

	app.router = newRouter()
	app.modifiers = Modifiers{}
	app.beforeModifiers = Modifiers{}

	return app
}

// Run starts a http.Server for the application with the given addr.
// This method blocks the calling goroutine.
func (a *Server) Run(addr string) {
	sort.Sort(a.modifiers)
	sort.Sort(a.beforeModifiers)

	http.ListenAndServe(addr, a)
}

// See http.Handler interface's ServeHTTP.
func (a *Server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	log.Println(req.RemoteAddr, req.Method, req.URL, req.UserAgent())

	for _, mod := range a.beforeModifiers {
		w, req = mod.fn(w, req)
		if req == nil {
			return
		}
	}

	node, req := a.router.resolve(req)
	if node == nil || node.fn == nil {
		if a.notFoundHandler != nil {
			a.notFoundHandler(w, req)
		} else {
			http.NotFound(w, req)
		}

		return
	}

	for _, mod := range a.modifiers {
		w, req = mod.fn(w, req)
		if req == nil {
			return
		}
	}

	for _, mod := range node.modifiers {
		w, req = mod.fn(w, req)
		if req == nil {
			return
		}
	}

	node.fn(w, req)
}

// AddModifier adds a modifier which is executed after the route is resolved.
func (a *Server) AddModifier(sort int, fn HandlerFunc) {
	a.modifiers = append(a.modifiers, Modifier{
		sort: sort,
		fn: fn,
	})
}

// AddBeforeModifier adds a modifier which is executed before the route is resolved.
func (a *Server) AddBeforeModifier(sort int, fn HandlerFunc) {
	a.beforeModifiers = append(a.beforeModifiers, Modifier{
		sort: sort,
		fn: fn,
	})
}

// GET adds a new request handler (HandlerFunc and modifiers) for a GET request with the given path.
func (a *Server) GET(path string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("GET", path, fn, modifiers)
}

// POST adds a new request handler (HandlerFunc and modifiers) for a POST request with the given path.
func (a *Server) POST(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("POST", route, fn, modifiers)
}

// PUT adds a new request handler (HandlerFunc and modifiers) for a PUT request with the given path.
func (a *Server) PUT(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("PUT", route, fn, modifiers)
}

// DELETE adds a new request handler (HandlerFunc and modifiers) for a DELETE request with the given path.
func (a *Server) DELETE(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("DELETE", route, fn, modifiers)
}

// PATCH adds a new request handler (HandlerFunc and modifiers) for a PATCH request with the given path.
func (a *Server) PATCH(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("PATCH", route, fn, modifiers)
}

// HEAD adds a new request handler (HandlerFunc and modifiers) for a HEAD request with the given path.
func (a *Server) HEAD(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("HEAD", route, fn, modifiers)
}

// OPTIONS adds a new request handler (HandlerFunc and modifiers) for a OPTIONS request with the given path.
func (a *Server) OPTIONS(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("OPTIONS", route, fn, modifiers)
}

// CONNECT adds a new request handler (HandlerFunc and modifiers) for a CONNECT request with the given path.
func (a *Server) CONNECT(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("CONNECT", route, fn, modifiers)
}

// TRACE adds a new request handler (HandlerFunc and modifiers) for a TRACE request with the given path.
func (a *Server) TRACE(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.router.addRoute("TRACE", route, fn, modifiers)
}

// Any adds a route for all HTTP methods.
func (a *Server) Any(route string, fn HandlerFunc, modifiers ...Modifier) {
	a.GET(route, fn, modifiers...)
	a.POST(route, fn, modifiers...)
	a.PUT(route, fn, modifiers...)
	a.DELETE(route, fn, modifiers...)
	a.PATCH(route, fn, modifiers...)
	a.HEAD(route, fn, modifiers...)
	a.OPTIONS(route, fn, modifiers...)
	a.CONNECT(route, fn, modifiers...)
	a.TRACE(route, fn, modifiers...)
}

// Group adds multiple routes with common modifiers and common path prefix.
func (a *Server) Group(path string, modifiers Modifiers, routes ...*Route) {
	for _, route := range routes {
		route.Path = "/" + strings.Trim(path, "/") + "/" + strings.TrimLeft(route.Path, "/")
		route.Modifiers = append(route.Modifiers, modifiers...)

		a.router.addRoute(route.Method, route.Path, route.Fn, route.Modifiers)
	}
}

// SetNotFoundHandler sets the HandlerFunc executed if no handler is found for the request.
func (a *Server) SetNotFoundHandler(fn HandlerFunc) {
	a.notFoundHandler = fn
}



//----------------------------------------------------------------------------------------------------------------------

// Modifier can change the request and the response before the route handler is called.
// The execution path is linear: Modifier1 -> Modifier2 -> RouteHandler.
// Create a new instance by using NewModifier().
type Modifier struct {
	sort int;
	fn   HandlerFunc
}

// NewModifier returns a new Modifier instance.
func NewModifier(sort int, fn HandlerFunc) Modifier {
	return Modifier{
		sort: sort,
		fn: fn,
	}
}



//----------------------------------------------------------------------------------------------------------------------

// Modifiers is a slice of Modifier implementing sort.Interface.
type Modifiers []Modifier

// See sort.Interface Len().
func (slice Modifiers) Len() int {
	return len(slice)
}

// See sort.Interface Less().
func (slice Modifiers) Less(i, j int) bool {
	return slice[i].sort < slice[j].sort;
}

// See sort.Interface Swap().
func (slice Modifiers) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

//----------------------------------------------------------------------------------------------------------------------

// Route is the route definition.
type Route struct {
	Fn        HandlerFunc
	Modifiers Modifiers
	Path      string
	Method    string
}



//----------------------------------------------------------------------------------------------------------------------

// router is a http request router.
// Create a new instance by using newRouter().
type router struct {
	trees methodTrees
}

// newRouter returns a router instance.
func newRouter() *router {
	r := &router{
		trees: make(methodTrees, 0, 9),
	}

	return r
}

// addRoute adds a new request handler / modifiers for a given method/path combination.
func (r *router) addRoute(method string, path string, fn HandlerFunc, modifiers Modifiers) {
	method = strings.ToUpper(method)
	sort.Sort(modifiers)

	root := r.trees.getRoot(method)
	if root == nil {
		root = &node{path:"/"}

		t := &tree{
			method: method,
			root: root,
		}

		r.trees = append(r.trees, t)
	}

	root.add(path, fn, modifiers)
}

// resolve returns the tree node and the request containing the context(if the route has parameters) for a given request.
// Returns nil,nil if no node was found for the request.
func (r *router) resolve(req *http.Request) (*node, *http.Request) {
	root := r.trees.getRoot(req.Method)
	if root == nil {
		return nil, nil
	}

	return root.resolve(req)
}

// dumpTree returns all trees as a string.
func (r *router) dumpTree() string {
	var str string
	for _, t := range r.trees {
		str += fmt.Sprintf("%s:\n", t.method)
		str += t.root.dump("")
		str += "\n\n"
	}

	return str
}



//----------------------------------------------------------------------------------------------------------------------

// node is a tree node for a specific path part.
type node struct {
	path      string
	children  []*node
	isDynamic bool
	fn        HandlerFunc
	modifiers Modifiers
}

// add adds a new node with a given path.
func (n *node) add(path string, fn HandlerFunc, modifiers Modifiers) {
	if path == "/" {
		n.path = "/"
		n.fn = fn
		n.modifiers = modifiers
		return
	}

	parts := strings.Split(path, "/")[1:]

	var resolvedNode *node
	for i := 0; i < len(parts); i++ {
		part := parts[i]
		resolvedNode = n.load(part)
		if resolvedNode == nil {
			resolvedNode = &node{
				path: part,
			}

			if len(part) > 0 && part[0] == ':' {
				resolvedNode.isDynamic = true
			}

			n.children = append(n.children, resolvedNode)
		}

		n = resolvedNode
	}

	resolvedNode.fn = fn
	resolvedNode.modifiers = modifiers
}

// resolve returns the node and the request with context for a given request.
// Returns nil, nil if no node was found for the request.
func (n *node) resolve(req *http.Request) (*node, *http.Request) {
	if req.URL.Path == "/" {
		return n, req
	}

	parts := strings.Split(req.URL.Path, "/")[1:]

	var ctx context.Context
	for i := 0; i < len(parts); i++ {
		part := parts[i]
		n = n.load(part)
		if n == nil {
			return nil, nil
		}

		if n.isDynamic {
			if ctx == nil {
				ctx = req.Context()
			}

			ctx = context.WithValue(ctx, n.path[1:], part)
		}
	}

	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return n, req
}

// load returns the child node matching the given path, nil if no matching node was found.
func (n *node) load(path string) *node {
	for _, node := range n.children {
		if node.path == path || node.isDynamic {
			return node
		}
	}

	return nil
}

// dump returns the node and its children as string.
func (n *node) dump(prefix string) string {
	line := fmt.Sprintf("%s%s\n", prefix, n.path)
	prefix += "  "
	for _, node := range n.children {
		line += node.dump(prefix)
	}

	return line
}



//----------------------------------------------------------------------------------------------------------------------

// tree contains the http method and the root node.
type tree struct {
	method string
	root   *node
}



//----------------------------------------------------------------------------------------------------------------------

// methodTrees is a slice of trees.
type methodTrees []*tree

// getRoot returns the root node for the tree with the given method, nil if no tree for the given method exists.
func (mt methodTrees) getRoot(method string) *node {
	for _, t := range mt {
		if t.method == method {
			return t.root
		}
	}

	return nil
}

