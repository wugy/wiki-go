package main

import (
	"log"
	"os"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

const DBFILE = "./db/wiki.db"

var db *sql.DB
var err error


func main() {
	log.Println("start")

	db, err = sql.Open("sqlite3", DBFILE)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	createDB()

	server := NewServer();

	server.AddBeforeModifier(0, OriginHandler)

	server.GET("/articles/:guid", ArticleGetHandler)
	server.PUT("/articles/:guid", ArticlePutHandler)
	server.POST("/articles/:guid", ArticlePutHandler)
	server.POST("/articles", ArticlePostHandler)

	server.GET("/navigation", NavigationHandler)

	server.Run(":81")
}

func createDB() {

	db.Exec("SELECT name FROM sqlite_master WHERE type='table' AND name='articles';")

	sqlStmt := `
		CREATE TABLE IF NOT EXISTS articles (
			guid text not null primary key,
			parent_guid text,
			name text not null,
			text text,
			html text,
			updatedate INTEGER not null
		);
		`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Fatal("createDB ERROR", err, sqlStmt)
		os.Exit(1)
	}
	log.Println("DB created")
}