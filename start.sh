#!/bin/bash

if [ ! -f "/code/glide.yaml"  ] || [ ! -d "/code/vendor"  ]; then
	rm /go/src -rf
	ln -s /code/vendor /go/src
	apk --update add gcc musl-dev build-base
	glide create
	glide get github.com/mattn/go-sqlite3
	glide get golang.org/x/net/context
	go install github.com/mattn/go-sqlite3
fi