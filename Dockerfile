FROM alpine

RUN apk --update add --no-cache ca-certificates

COPY wiki-go /wiki-go

CMD /wiki-go