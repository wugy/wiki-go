package main

import (
	"net/http"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"database/sql"
	"log"
	"io"
	"crypto/rand"
	"encoding/json"
	"bytes"
	"io/ioutil"
)

type Article struct {
	GUID       string        `json:"guid"`
	Parent	   string	 `json:"parent"`
	Name       string        `json:"name"`
	Text       string        `json:"text"`
	HTML       string        `json:"html"`
	LastUpdate uint          `json:"lastupdate"`
}
type ArticleNavi struct {
	GUID    string                `json:"guid"`
	Name    string                `json:"name"`
	Childs []*ArticleNavi         `json:"childs"`
}

func checkErr(err error) {
	if err != nil {
		log.Println("Error ", err)
	}
}

func OriginHandler(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	return w,r
}

/***************************************************************************************************
 * Article
 **************************************************************************************************/

func ArticleGetHandler(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	row := db.QueryRow("SELECT guid, name, text, html, updatedate, parent_guid FROM articles WHERE guid = ?", r.Context().Value("guid"))
	article := &Article{}
	row.Scan(&article.GUID, &article.Name, &article.Text, &article.HTML, &article.LastUpdate, &article.Parent)

	json, err := json.Marshal(article)
	checkErr(err)

	fmt.Fprint(w, string(json))

	return w, r
}

func ArticlePutHandler(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	stmt, err := db.Prepare("UPDATE articles SET parent_guid = ?, name = ?, text = ?, html = ?, updatedate = strftime('%s','now') WHERE guid = ?")
	checkErr(err)
	res, err := stmt.Exec(r.FormValue("parent_guid"), r.FormValue("name"), r.FormValue("text"), getHTML(r.FormValue("text")), r.Context().Value("guid"))
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	log.Println("Rows Updated NAME - ", affect)

	fmt.Fprint(w, `{"success": true}`)

	return w, r
}

func ArticlePostHandler(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	stmt, err := db.Prepare("INSERT INTO articles (guid, parent_guid, text, html, name, updatedate) VALUES (?,?,?,?,?,strftime('%s','now'))")
	checkErr(err)
	defer stmt.Close()

	// Gen GUID
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return w, r
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8] &^ 0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6] &^ 0xf0 | 0x40
	guid := fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])

	if len(r.FormValue("parent_guid")) > 0 {
		_, err = stmt.Exec(guid, r.FormValue("parent_guid"), r.FormValue("text"), getHTML(r.FormValue("text")), r.FormValue("name"))
	} else {
		_, err = stmt.Exec(guid, nil, r.FormValue("text"), getHTML(r.FormValue("text")), r.FormValue("name"))
	}

	checkErr(err)

	log.Println(guid)

	fmt.Fprint(w, `{"guid": "` + guid + `"}`)

	return w, r
}

func getHTML(text string) string {
	values := map[string]string{"text": text, "mode": "markdown"}
	jsonValue, _ := json.Marshal(values)
	resp, err := http.Post("https://api.github.com/markdown?access_token=26847018963ab258ba1ddb08692e79f753732539", "application/json", bytes.NewBuffer(jsonValue))
	checkErr(err)

	htmlData, err := ioutil.ReadAll(resp.Body) //<--- here!
	checkErr(err)

	return string(htmlData)
}

/***************************************************************************************************
 * Navigation
 **************************************************************************************************/

func NavigationHandler(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	var articles []*ArticleNavi = []*ArticleNavi{}

	articles = getChilds("")

	naviJson, err := json.Marshal(articles)
	checkErr(err)

	fmt.Fprint(w, string(naviJson))

	return w, r
}

func getChilds(guid string) []*ArticleNavi {

	var articles []*ArticleNavi = []*ArticleNavi{}

	var rows *sql.Rows
	if len(guid) > 0 {
		rows, err = db.Query("SELECT guid, name FROM articles WHERE parent_guid = ? ORDER BY name", guid)
	} else {
		rows, err = db.Query("SELECT guid, name FROM articles WHERE parent_guid IS NULL ORDER BY name")
	}
	checkErr(err)

	defer rows.Close()
	for rows.Next() {
		article := &ArticleNavi{}
		err = rows.Scan(&article.GUID, &article.Name)
		checkErr(err)
		article.Childs = getChilds(article.GUID)
		articles = append(articles, article)
	}
	err = rows.Err()
	checkErr(err)

	return articles
}